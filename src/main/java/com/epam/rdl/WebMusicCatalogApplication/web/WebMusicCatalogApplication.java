package com.epam.rdl.WebMusicCatalogApplication.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebMusicCatalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebMusicCatalogApplication.class, args);
	}

}
